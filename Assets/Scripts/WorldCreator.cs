﻿using System.Collections.Generic;
using UnityEngine;

public class WorldCreator : MonoBehaviour
{
    // --- Спавн дороги ---
    public Transform player;                                           //позиция игрока
    public GameObject[] tilePrefabs;                                   //префабы дороги - тайлы  
    public float tileLength = 24f;                                     //длина тайла
    public float distanceToReload = 500f;                              //расстояние, на котором мир сместится в нуль
    public int numberOfTiles = 5;                                      //количество тайлов на карте одновременно
    private float zSpawn = 0f;                                         //следующая позиция для спавна
    private List<GameObject> activeTiles = new List<GameObject>();     //ссылки на тайлы на карте
    public Transform motherOfTiles;                                    //контейнер для тайлов


    void Start()
    {
        SpawnTile(-1);      //создание начальных n тайлов
    }
    
    void Update()
    {
        //спавн после проезда некого расстояния
        if (player.position.z - (tileLength + 5) > zSpawn - (numberOfTiles * tileLength))
        {
            SpawnTile(Random.Range(0, tilePrefabs.Length));
            DeleteTile();
        }

        //перемещение всех объектов при достижении некого расстояния
        if (player.transform.position.z > distanceToReload)
            MoveWorld();
    }


    //спавн части дороги или всей дороги(-1)
    public void SpawnTile(int tileIndex)
    {
        if (tileIndex == -1)        //создание карты в начале
        {
            for (int i = 0; i < numberOfTiles; i++)
                SpawnTile(Random.Range(0, tilePrefabs.Length));
            
        }
        else                        //создание карты после начала
        {
            GameObject go = Instantiate(tilePrefabs[tileIndex], transform.forward * zSpawn, transform.rotation);
            go.transform.SetParent(motherOfTiles);
            activeTiles.Add(go);
            zSpawn += tileLength;
        }
    }

    //удаление конкретной части дороги или всей дороги(-1)
    private void DeleteTile(int num = 1)
    {
        if (num == -1)
        {
            for (int i = 0; i < numberOfTiles; i++)
            {
                Destroy(activeTiles[0]);     //удаляем со сцены
                activeTiles.RemoveAt(0);     //удаляем со списка
            }
            zSpawn = 0;
        }
        else
        {
            Destroy(activeTiles[0]);     //удаляем со сцены
            activeTiles.RemoveAt(0);     //удаляем со списка
        }
    }

    
    void MoveWorld()
    {
        print("lapWorld");

        DeleteTile(-1);
        SpawnTile(-1);

        player.position = new Vector3(player.position.x, player.position.y, 0f);
    }

    
}